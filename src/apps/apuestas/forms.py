# -*- coding: utf-8 -*-
from django import forms

from .models import Pregunta, Apuesta


class PreguntaForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(PreguntaForm, self).__init__(*args, **kwargs)

    class Meta:
        model = Pregunta
        fields = ('text', 'due_date')
        exclude = ('create_date', 'create_user', 'update_date', 'update_user')
        widgets = {
            'text': forms.TextInput(attrs={
                'placeholder': 'Ingrese la pregunta.',
                'class': 'form-control'
            }),
            'due_date': forms.TextInput(attrs={
                'class': 'form-control'
            }),
        }

    def save(self, commit=True):
        return super(PreguntaForm, self).save(commit=commit)


class ApuestaForm(forms.ModelForm):

    pregunta = None

    def __init__(self, pregunta, *args, **kwargs):
        super(ApuestaForm, self).__init__(*args, **kwargs)
        self.pregunta = pregunta
        self.fields['respuesta_validas'].label = self.pregunta.text
        self.fields['respuesta_validas'].queryset = self.pregunta.respuesta_validas

    class Meta:
        model = Apuesta
        # Como son todos campos, no queremos que se muestren en el formulario
        # lo dejamos para el template
        exclude = ('pregunta', 'date', 'user')
        """
        widgets = {
            'user': forms.Textarea(attrs={
                'class': 'form-control'
            }),
            'pregunta': forms.Select(attrs={
                'class': 'form-control'
            }),
            'respuesta': forms.TextInput(attrs={
                'placeholder': 'Ingrese la respuesta.',
                'class': 'form-control'
            }),
        }
        """
        widgets = {
            'respuesta_valida': forms.Select(attrs={
                'placeholder': 'Ingrese la pregunta.',
                'class': 'form-control'
            }),
        }

    def clean_respuesta_valida(self):
        if Apuesta.objects.filter(pregunta=self.pregunta, user=self.user).exists():
            raise forms.ValidationError('Ei sos tonto, ya contestaste esto.')
        else:
            return self.cleaned_data['respuesta_valida']
