# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from datetime import datetime

from django.test import TestCase
from django.contrib.auth.models import User

from .models import Pregunta, Respuesta


class ApuestasTest(TestCase):

    def setUp(self):
        user = User(username="Pedor")
        user.save()

    def test_crear_pregunta(self):
        user = User.objects.get(username="Pedor")
        pregunta = Pregunta(
            text='¿caerá piedra mañana?',
            due_date=datetime.now(),
            create_user=user,
            update_user=user,
        )
        pregunta.save()


class RespuestaTest(TestCase):

    def setUp(self):
        user = User(username="Pedor")
        user.save()
        pregunta = Pregunta(
            text='¿caerá piedra mañana?',
            due_date=datetime.now(),
            create_user=user,
            update_user=user,
        )
        pregunta.save()

    def test_crear_respuesta(self):
        user = User.objects.get(username="Pedor")
        # print object(user.username)
        # print type('Pedor')
        self.assertIs(user.username, 'Pedor')
        pregunta = Pregunta.objects.get(text='¿caerá piedra mañana?')
        respuesta = Respuesta(
            pregunta=pregunta,
            text='Si',
            create_user=user,
            update_user=user,
        )
        respuesta.save()
        respuesta = Respuesta(
            pregunta=pregunta,
            text='No',
            create_user=user,
            update_user=user,
        )
        respuesta.save()

    def test_se_X(self):
        pass

    def test_login_post(self):
        from django.contrib.auth.forms import UserCreationFrom
        form = UserCreationFrom(
            data={
                'username': 'dot',
                'password1': 'password2',
                'password2': 'password2'
            }
        )
        self.assertTrue(form.is_valid)
        client = Client()
        response = client.post(
            '/accounts/login/',
            {
                'username': 'dot',
                'password': ''
            }
        )
        self.assertFormError(response, 'form', 'password', 'This field is required.')
