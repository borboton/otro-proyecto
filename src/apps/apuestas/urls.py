from django.conf.urls import url

from .views import PreguntaView, index, apuesta

urlpatterns = [
    url(r'^$', index, name='apuestas_index'),
    url(r'^(?P<id_pregunta>\d+)/$', PreguntaView.as_view()),
    url(r'^(?P<id_apuesta>\d+)/$', apuesta, name="apuesta")

]
