# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import Pregunta, Respuesta, Apuesta


def add_simbol_text(queryset):
    for q in queryset:
        q.text = q.text + '?'
        q.save()
add_simbol_text.short_description = 'Agregar simbolo ?'


class RespuestaValidasHeadInLine(admin.TabularInline):
    model = Respuesta
    extra = 2
    # readonly_fields = ('create_user', 'update_user')


class PreguntaAdmin(admin.ModelAdmin):
    list_display = ('id', 'text', 'create_user')
    search_fields = ('text', 'create_user__username')
    list_filter = ('text',)
    inlines = [RespuestaValidasHeadInLine, ]
    actions = [add_simbol_text, ]

admin.site.register(Pregunta, PreguntaAdmin)


class ApuestaAdmin(admin.ModelAdmin):
    list_display = ('id', 'pregunta', 'respuesta', 'create_user')
    # list_filter = ('respuesta__text',)

admin.site.register(Apuesta, ApuestaAdmin)


class RespuestaAdmin(admin.ModelAdmin):
    list_display = ('id', 'pregunta', 'text')

admin.site.register(Respuesta, RespuestaAdmin)
