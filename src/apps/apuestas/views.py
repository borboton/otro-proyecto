# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import json
import urllib
import urllib2

from django.conf import settings
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.shortcuts import render
from django.views import View
from django.template import loader
from django.http import HttpResponse

from .forms import PreguntaForm, ApuestaForm
from .models import Pregunta


def get_apuestas_api():
    handler = urllib2.HTTPHandler()
    opener = urllib2.build_opener(handler)
    request = urllib2.Request(settings.HOST_API_GET)
    request.add_header('Content-Type', 'application/json')
    request.add_header('Authorization', 'token %s' % settings.API_AUTH_TOKEN)
    try:
        connection = opener.open(request)
    except urllib2.HTTPError as e:
        connection = e

    if connection.code == 200:
        data = connection.read()
        return json.loads(data)
    else:
        pass


def set_apuestas_api(respuesta_id):
    method = "POST"
    handler = urllib2.HTTPHandler()
    opener = urllib2.build_opener(handler)
    data = urllib.urlencode({'respuesta_id': respuesta_id})
    request = urllib2.Request(settings.HOST_API + 'registrar_apuesta/', data=data)
    request = urllib2.Request(settings.HOST_API_POST)
    request.add_header('Content-Type', 'application/json')
    request.add_header('Authorization', 'token %s' % settings.API_AUTH_TOKEN)
    request.get_method = lambda: method
    try:
        connection = opener.open(request)
    except urllib2.HTTPError as e:
        connection = e

    if connection.code == 200:
        data = connection.read()
        return json.loads(data)
    else:
        pass


@login_required
def index(request):
    form = PreguntaForm()
    if request.method == 'POST':
        # return HttpResponse("<h2>Thanks</h2>")
        form = PreguntaForm(data=request.POST)
        if form.is_valid():
            pregunta = form.save(commit=False)
            pregunta.create_user = request.user
            pregunta.update_user = request.user
            pregunta.save()
    template = loader.get_template('apuestas_index.html')
    pregunta_list = Pregunta.objects.all()
    context = {
        'username': request.user.username,
        'form': form,
        'pregunta_list': pregunta_list,
        'apuesta_list_api': get_apuestas_api(),
    }
    return HttpResponse(template.render(context, request))


@login_required
def apuesta(request, id_pregunta):
    template = loader.get_template('apuesta.html')
    pregunta = Pregunta.objects.get(pk=id_pregunta)
    form = ApuestaForm(pregunta)
    if request.method == 'POST':
        form = ApuestaForm(pregunta, data=request.POST)
        if form.is_valid():
            respuesta_apuesta = form.save(commit=False)
            respuesta_apuesta.user = request.user
            apuesta.save()
            return HttpResponse(template.render(context, request))
    context = {
        'pregunta': pregunta,
        # no hace falta el username, porque mandamos el request al template
        # y por ende lo podemos tener de ahi
        # 'username': request.user.username,
        'form': form
    }
    return HttpResponse(template.render(context, request))


class PreguntaView(LoginRequiredMixin, View):
    form = PreguntaForm()
    template_name = "pregunta_form.html"

    def get(self, request):
        id_pregunta = self.kwargs['id_pregunta']
        prg = Pregunta.objects.get(pk=id_pregunta)
        form = PreguntaForm(instance=prg)
        context = {
            'form': form
        }
        return render(request, self.template_name, context)

    def post(self, request):
        id_pregunta = self.kwargs['id_pregunta']
        prg = Pregunta.objects.get(pk=id_pregunta)
        form = PreguntaForm(data=request.POST, instance=prg)
        if form.is_valid():
            prg = form.save(commit=False)
            prg.save()
        context = {
            'form': form
        }
        return render(request, self.template_name, context)
