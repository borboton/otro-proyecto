from django.core.management.base import BaseCommand
from django.core import mail
from django.conf import settings

from apps.apuestas.models import Respuesta


class Command(BaseCommand):

    def handle(self, *args, **options):
        connection = mail.get_connection()
        import ipdb; ipdb.set_trace()
        connection.open()

        e_mail = mail.EmailMessage(
            'Hello',
            'Body goes here',
            settings.EMAIL_HOST_USER,
            [settings.EMAIL_TO_SEND],
            connection=connection
        )
        e_mail.send()

        e_mail_1 = mail.EmailMessage(
            'Hello',
            'Body goes here',
            settings.EMAIL_HOST_USER,
            ['rubengocio@gmail.com'],
            connection=connection
        )
        e_mail_1.send()

        e_mail_2 = mail.EmailMessage(
            'Hello',
            'Body goes here',
            settings.EMAIL_HOST_USER,
            ['pdalmasso@gmail.com'],
            connection=connection
        )

        e_mail_3 = mail.EmailMessage(
            'Hello',
            'Body goes here',
            settings.EMAIL_HOST_USER,
            ['pdalmasso@gmail.com'],
            connection=connection
        )

        connection.send_messages([e_mail_2, e_mail_3])
        connection.close()

    # def send_winners_email(self):
    #     mail.send_mass_mail(
    #         'Has ganado!',
    #         'Bien hecho sopenco.',
    #         'settings.EMAIL_HOST_USER',
    #         list(Respuesta.objects.filter('correct').values('correct_answer')),
    #         #lista de dic, no se si funciona eso!
    #         fail_silently=False,
    #     )
