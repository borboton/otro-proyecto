from zeep import Client
from zeep.cache import SqliteCache
from zeep.transports import Transport
# from zeep imoprt xsd


cache = SqliteCache(path='/tmp/sqlite.db', timeout=60)
transport = Transport(cache=cache)
client = Client(
    'http://www.webservicex.net/ConvertSpeed.asmx?WSDL',
    transport=transport)


# The ServiceProxy object

# service is a ServiceProxy object.  It will check if there
# is an operation with the name `X` defined in the binding
# and if that is the case it will return an OperationProxy
client.service.X()

# The operation can also be called via an __getitem__ call.
# This is usefull if the operation name is not a valid
# python attribute name.
client.service['X-Y']()


# Settings options

with client.options(raw_response=True):
    response = client.service.myoperation()

    # response is now a regular requests.Response object
    assert response.status_code == 200
    assert response.content

