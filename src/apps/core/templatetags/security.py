from django import template
# from django.contrib.auth import *

register = template.Library()

@register.filter(name="have_player_group")
def have_player_group(user):
    return user.groups.filter(name="Jugador").exists()

@register.filter(name="have_admin_group")
def have_admin_group(user):
    return user.groups.filter(name="Admin").exists()
