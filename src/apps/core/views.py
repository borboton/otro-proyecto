# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import Group
from django.http import HttpResponse
from django.template import loader
from django.shortcuts import render

from .functions import JsonResponse


def register_user(request):
    template = loader.get_template('registration/register.html')
    form = UserCreationForm()
    if request.method == 'POST':
        # validar los datos y guardarlos
        form = UserCreationForm(data=request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.is_active = True
            player_group = Group.objects.get(name='Jugador')
            user.save()
            # el add es a nivel de base de datos, por lo tanto no hace falta
            # el save, el add ya lo hace
            user.groups.add(player_group)
            response = JsonResponse().response_ok()
        else:
            response = JsonResponse().respose_error_form(form)
        return response
    context = {
        'form': form
    }
    return HttpResponse(template.render(context, request))


def azenda_button(request):
    return render(request, 'azenda.html')

