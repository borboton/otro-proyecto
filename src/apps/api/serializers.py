# -*- coding: utf-8 -*-
from django.apps import apps
from django.contrib.auth.models import User

from rest_framework import serializers


class PreguntasSerializer(serializers.HyperlinkedModelSerializer):
    text = serializers.CharField()
#   due_date = serializers.DateTimeField()
#   create_date = serializers.DateTimeField()
#   create_user = serializers.PrimaryKeyRelatedField(queryset=User.objects.all())
#   update_date = serializers.DateTimeField()
#   update_user = serializers.PrimaryKeyRelatedField(queryset=User.objects.all())

    class Meta:
        Pregunta = apps.get_model('apuestas', 'Pregunta')
        model = Pregunta
        fields = ('text', )


class RespuestasSerializer(serializers.HyperlinkedModelSerializer):
    text = serializers.CharField()

    class Meta:
        Respuesta = apps.get_model('apuestas', 'Respuesta')
        model = Respuesta
        fields = ('text', )


class ApuestasSerializer(serializers.HyperlinkedModelSerializer):
    create_user = serializers.PrimaryKeyRelatedField(queryset=User.objects.all())
    pregunta = PreguntasSerializer()
    respuesta = RespuestasSerializer()
#   date = serializers.DateTimeField()

    class Meta:
        Apuesta = apps.get_model('apuestas', 'Apuesta')
        model = Apuesta
        fields = ('id', 'respuesta', 'pregunta', 'create_user')
