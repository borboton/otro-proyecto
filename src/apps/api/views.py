# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import apps

from rest_framework import viewsets
from rest_framework import permissions
from rest_framework.decorators import api_view
from rest_framework.response import Response

from .serializers import ApuestasSerializer, RespuestasSerializer, PreguntasSerializer


class ApuestasViewSet(viewsets.ModelViewSet):
    queryset = apps.get_model('apuestas', 'Apuesta').objects.all()
    serializer_class = ApuestasSerializer
    permission_classes = (permissions.IsAuthenticated,)


class RespuestasViewSet(viewsets.ModelViewSet):
    queryset = apps.get_model('apuestas', 'Respuesta').objects.all()
    serializer_class = RespuestasSerializer
    permission_classes = (permissions.IsAuthenticated,)


class PreguntasViewSet(viewsets.ModelViewSet):
    queryset = apps.get_model('apuestas', 'Pregunta').objects.all()
    serializer_class = PreguntasSerializer
    permission_classes = (permissions.IsAuthenticated,)


@api_view(['POST'])
def registrar_apuesta(request):
    if request.method == 'POST':
        usuario_id = request.POST.get('usuario_id', 0)
        respuesta_id = request.POST.get('respuesta_id', 0)
        Apuesta = apps.get_model('apuestas', 'Apuesta')
        # user = User.objects.get(pk=request.user.id)
        respuesta_validas = apps.get_model('apuestas', 'Respuesta').objects.get(pk=respuesta_id)
        apuesta = Apuesta(
            respuesta_valida=respuesta_validas,
            user=usuario_id
        )
        apuesta.save()
        return Response({
            'status': 'ok'
        })
    return Response({
        "status": "error",
        "response": ""
    })
