# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from apuestas import settings


class Read(models.Model):
    name = models.CharField(max_length=100)
    link = models.URLField()
    text = models.TextField()
    img = models.ImageField(upload_to=settings.LINK_ICONS)

    def get_absolute_url(self):
        return "%s" % self.link
