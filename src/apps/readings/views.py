# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.views.generic import ListView

from .models import Read


class Readings(ListView):
    model = Read
    template_name = "read.html"
    context_object_name = 'reading_list'
