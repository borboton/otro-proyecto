from django.conf.urls import url

from .views import Readings

urlpatterns = [
    url(r'^', Readings.as_view(), name='read'),
]
