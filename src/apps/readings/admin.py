# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from .models import Read


class ReadAdmin(admin.ModelAdmin):
    list_display = [f.name for f in Read._meta.get_fields()]
    search_fields = ('text', 'name')


admin.site.register(Read, ReadAdmin)
