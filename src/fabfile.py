# -*- coding: utf-8 -*-
from fabric.api import env, run, prefix
from fabric.context_managers import cd
from fabric.utils import puts
from fabric.colors import green, blue
from fab_tools.fab_settings import USER, PASSWORD, PATH_PROJECT, PATH_PROJECT_SRC, PATH_VENV_ACTIVATE
from fabric.operations import sudo


def test():
    env.hosts = ["127.0.0.1:8000"]
    env.user = USER['test']
    env.PASSWORD = PASSWORD['test']
    env.enviroment = "test"


def prod():
    env.hosts = ["arizzo.tuxis.com.ar"]
    env.user = USER['prod']
    env.PASSWORD = PASSWORD['prod']
    env.enviroment = "prod"


def deploy():
    puts(green("Host: %s" % env.hosts[0]))
    puts(green("Entorno: %s" % env.enviroment))
    puts(green("Usuario: %s" % env.user))
    # confirmation = raw_input('Continuar (s/n): ')
    # if confirmation.lower() == 's':
    with cd(PATH_PROJECT[env.enviroment]):
        puts(blue('Descargando ultima versión...'))
        run('git pull origin master')
    with cd(PATH_PROJECT_SRC[env.enviroment]):
        puts(blue('Eliminando temporales...'))
        run('find . -name \'*.pyc\' -delete')
        with prefix(PATH_VENV_ACTIVATE[env.enviroment]):
            puts(blue('Instalando requerimientos...'))
            run('pip install -r ../doc/requirements.txt')
            #puts(blue('Borrando archivos estaticos...'))
            #run('rm static -R')
            puts(blue('Recolectando archivos estaticos...'))
            run('python manage.py collectstatic -l --noinput')
            puts(blue('Cargando base de datos...'))
            run('python manage.py loaddata db.json')
            puts(blue('Corriendo migraciones...'))
            run('python manage.py migrate -v 1')
    with cd(PATH_PROJECT[env.enviroment]):
        run('chmod 777 %s' % PATH_PROJECT[env.enviroment])
    puts(blue('Reiniciando el servidor...'))
    restart_apache()


def restart_apache():
    sudo('service apache2 restart')
