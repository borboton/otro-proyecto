USER = {
    'test':'',
    'prod':''
}

PASSWORD = {
    'test':'',
    'prod':''
}

PATH_PROJECT = {
    'test': PATH_PROJECT['test'] + '/src',,
    'prod': PATH_PROJECT['prod'] + '/src'
}

PATH_VENV = {
    'test': PATH_PROJECT['test'] + '/v',
    'prod': PATH_PROJECT['prod'] + '/v'
}

PATH_VENV_ACTIVATE = {
    'test': PATH_VENV['test'] + '/bin/activate',
    'prod': PATH_VENV['prod'] + '/bin/activate'
}
